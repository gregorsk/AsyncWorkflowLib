﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using AsyncWorkflow;

namespace WpfAsyncWorkflowApp.Converters {
	public class StepStatusToDisplayConverter: IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			if (value == null || value.GetType() != typeof(StepStatus)) {
				return null;
			}

			var status = (StepStatus) value;
			switch (status) {
				case StepStatus.NotRun:
					return Application.Current.FindResource("StatusNotStarted");
				case StepStatus.Success:
					return Application.Current.FindResource("StatusOk");
				case StepStatus.Failed:
					return Application.Current.FindResource("StatusError");
				default:
					return null;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}
