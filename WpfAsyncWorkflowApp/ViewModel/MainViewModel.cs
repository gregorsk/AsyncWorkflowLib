using System.Collections.ObjectModel;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using AsyncWorkflow;
using GalaSoft.MvvmLight.Command;

namespace WpfAsyncWorkflowApp.ViewModel {
	/// <summary>
	/// This class contains properties that the main View can data bind to.
	/// <para>
	/// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
	/// </para>
	/// <para>
	/// You can also use Blend to data bind with the tool's support.
	/// </para>
	/// <para>
	/// See http://www.galasoft.ch/mvvm
	/// </para>
	/// </summary>
	public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
			if (IsInDesignMode) {
				Steps.Add(new WorkflowStep<object>("step1"));
				Steps.Add(new WorkflowStep<object>("step2"));
			}
			////else
			////{
			////    // Code runs "for real"
			////}
		}
	
		public ObservableCollection<WorkflowStep> Steps { get; } = new ObservableCollection<WorkflowStep>();

	    private RelayCommand _loadStepsCommand;
	    public RelayCommand LoadStepsCommand {
		    get {
				return _loadStepsCommand ?? (_loadStepsCommand = new RelayCommand(() => {
					var i = 0;
				    var step1 = new WorkflowStep<string>("step1", async (step, setStepResult) => {
					    await Task.Delay(1000);
					    setStepResult($"sync call completed {++i}");
				        return StepStatus.Success;
				    });
                    var step2 = new WorkflowStep<bool>("step2", async (step, setStepResult) => {
	                    await Task.Run(async () => {
		                    await Task.Delay(1000);
	                    });
	                    setStepResult(true);
	                    return StepStatus.Success;
                    });
				    var step3 = new WorkflowStep<string>("step3", (step, setStepResult) => {
				        setStepResult("OK");
				        return Task.FromResult(StepStatus.Success);
				    });

				    var step4 = new WorkflowStep<int>("step4", (step, setStepResult) => {
					    setStepResult(2);
					    return Task.FromResult(StepStatus.Success);
				    });

				    var step5 = new WorkflowStepWithRollback<float>("step5", (step, setStepResult) => {
				        setStepResult(21.5f);
				        return Task.FromResult(StepStatus.Failed);
				    });

                    step5.RollbackStep = new WorkflowStep("step5 rollback", ()=> Task.FromResult(StepStatus.Success));

                    var step6 = new WorkflowStep<object>("step6", (step, setStepResult) => {
                        setStepResult("lalal");
                        return Task.FromResult(StepStatus.Success);
                    });

                    Steps.Add(step1);
				    Steps.Add(step2);
				    Steps.Add(step3);
                    Steps.Add(step4);
                    Steps.Add(step5);
                    Steps.Add(step6);
					Steps.Add(step5.RollbackStep);

					int j = 3, k=2;
				    step1.Configure()
				        .RunIf(()=> true)
					    .When((step)=> --k > 0).RepeatTimes(1)
				        .When(StepStatus.Success).GotoStep(step2)
				        .When(StepStatus.Failed).RepeatTimes(3)
				        .When(StepStatus.Failed).GotoStep(step3)
				        .When(StepStatus.NotRun).GotoStep(step5);
				    step2.Configure()
				        .When(StepStatus.Success).GotoStep(step3)
				        .When(StepStatus.Failed).RepeatTimes(3)
				        .DefaultNextStep(step4);
					step3.Configure()
					    .DefaultNextStep(step4);
				    step4.Configure()
				        .DefaultNextStep(step5);
				    step5.Configure()
				        .When(StepStatus.Success).GotoStep(step6)
				        .When(StepStatus.Failed).GotoStep(step5.RollbackStep);    
				}));
		    }
	    }

	    private RelayCommand _runWorkflowCommand;
	    public RelayCommand RunWorkflowCommand {
		    get {
			    return _runWorkflowCommand ?? (_runWorkflowCommand = new RelayCommand(async () => {
			        var currentStep = Steps[0];
			        while (currentStep != null) {
			            await currentStep.RunAsync();
			            currentStep = currentStep.GetNextStep();
			        }
			    }));
		    }
	    }

	}
}