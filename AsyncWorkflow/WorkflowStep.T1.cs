﻿using System;
using System.Threading.Tasks;

namespace AsyncWorkflow {
	public delegate void SetStepResult<in T>(T stepResult);

	/// <inheritdoc cref="WorkflowStep" />
	/// <summary>
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class WorkflowStep<T>: WorkflowStep, IConfigurableStep<WorkflowStep<T>> {
		public WorkflowStep(string stepName, Func<Task<StepStatus>> asyncFunc = null) : base(stepName, asyncFunc) { }

		public WorkflowStep(string stepName, Func<WorkflowStep<T>, SetStepResult<T>, Task<StepStatus>> asyncFunc) : base(stepName) {
			SetStepFunction(asyncFunc);
		}

		public T StepResult { get; private set; }

		protected void SetStepFunction(Func<WorkflowStep<T>, SetStepResult<T>, Task<StepStatus>> asyncFunc) {
			if (asyncFunc == null) {
				base.SetStepFunction(null);
				return;
			}

			SetStepFunction(async () => {
				var status = await asyncFunc(this, result => {
					StepResult = result;
					OnPropertyChanged(nameof(StepResult));
				});
				return status;
			});
		}

		#region Implementation of IStepCondition<T>

		IStepConditionResult<WorkflowStep<T>> IStepCondition<WorkflowStep<T>>.When(StepStatus status) {
			return (WorkflowStep<T>)((IStepCondition<WorkflowStep>) this).When(status);
		}

		IStepConditionResult<WorkflowStep<T>> IStepCondition<WorkflowStep<T>>.When(Func<WorkflowStep<T>, bool> condition) {
			return (WorkflowStep<T>)((IStepCondition<WorkflowStep>) this).When((s) => condition(this));
		}

		IStepCondition<WorkflowStep<T>> IStepCondition<WorkflowStep<T>>.RunIf(Func<bool> condition) {
			return (WorkflowStep<T>)((IStepCondition<WorkflowStep>) this).RunIf(condition);
		}

		IStepCondition<WorkflowStep<T>> IStepCondition<WorkflowStep<T>>.SkipThisStep() {
			return (WorkflowStep<T>)((IStepCondition<WorkflowStep>) this).SkipThisStep();
		}

		IStepCondition<WorkflowStep<T>> IStepCondition<WorkflowStep<T>>.DefaultNextStep(WorkflowStep nextStep) {
			return (WorkflowStep<T>)((IStepCondition<WorkflowStep>) this).DefaultNextStep(nextStep);
		}

		#endregion

		#region Implementation of IStepConditionResult<T>

		IStepCondition<WorkflowStep<T>> IStepConditionResult<WorkflowStep<T>>.GotoStep(WorkflowStep nextStep) {
			return (WorkflowStep<T>)((IStepConditionResult<WorkflowStep>) this).GotoStep(nextStep);
		}

		IStepCondition<WorkflowStep<T>> IStepConditionResult<WorkflowStep<T>>.RepeatTimes(int i) {
			return (WorkflowStep<T>)((IStepConditionResult<WorkflowStep>) this).RepeatTimes(i);
		}

		#endregion

		#region Implementation of IConfigurableStep<T>

		public new IStepCondition<WorkflowStep<T>> Configure() {
			return (WorkflowStep<T>)((WorkflowStep) this).Configure();
		}

		#endregion
	}
}