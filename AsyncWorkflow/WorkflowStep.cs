﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AsyncWorkflow {
	/// <summary>
    /// 
    /// </summary>
    public class WorkflowStep : INotifyPropertyChanged, IConfigurableStep<WorkflowStep> {
		private WorkflowStep _defaultNextStep;
		private Func<bool> _runCondition;
		private Func<Task<StepStatus>> _stepFunction;
		private readonly Dictionary<Func<bool>, WorkflowStep> _nextStepsMap = new Dictionary<Func<bool>, WorkflowStep>();
		private Func<bool> _curentExitCondition;

		public WorkflowStep(string stepName, Func<Task<StepStatus>> asyncFunc = null) {
            StepName = stepName;
		    SetStepFunction(asyncFunc);
	    }

        public string StepName { get; }

		public StepStatus Status { get; private set; } = StepStatus.NotRun;

		public virtual async Task RunAsync() {
			if (_stepFunction != null && (_runCondition == null || (_runCondition != null && _runCondition()))) {
				var status = await _stepFunction();
				Status = status;
				OnPropertyChanged(nameof(Status));
			}
		}

        public virtual WorkflowStep GetNextStep() {
            foreach (var i in _nextStepsMap) {
                if (i.Key()) {
                    return i.Value;
                }
            }
            return _defaultNextStep;
        }

		protected void SetStepFunction(Func<Task<StepStatus>> asyncFunc) {
			_stepFunction = asyncFunc;
		}

        #region Implementation of INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Implementation of IStepCondition

        IStepCondition<WorkflowStep> IStepCondition<WorkflowStep>.RunIf(Func<bool> conditionFunc) {
            _runCondition = conditionFunc;
            return this;
        }

        IStepCondition<WorkflowStep> IStepCondition<WorkflowStep>.SkipThisStep() {
            _runCondition = () => false;
            return this;
        }

		IStepConditionResult<WorkflowStep> IStepCondition<WorkflowStep>.When(Func<WorkflowStep, bool> condition) {
			_curentExitCondition = () => condition(this);
			return this;
		}

		IStepConditionResult<WorkflowStep> IStepCondition<WorkflowStep>.When(StepStatus status) {
            _curentExitCondition = () => Status == status;
            return this;
        }

		IStepCondition<WorkflowStep> IStepCondition<WorkflowStep>.DefaultNextStep(WorkflowStep nextStep) {
			_defaultNextStep = nextStep;
			return this;
		}

        #endregion

        #region Implementation of IStepConditionResult

        IStepCondition<WorkflowStep> IStepConditionResult<WorkflowStep>.GotoStep(WorkflowStep nextStep) {
            _nextStepsMap.Add(_curentExitCondition, nextStep);
            return this;
        }

        IStepCondition<WorkflowStep> IStepConditionResult<WorkflowStep>.RepeatTimes(int i) {
            _nextStepsMap.Add(() => !_curentExitCondition() && --i >= 0, this);
            return this;
        }
        #endregion

        #region Implementation of IConfigurableStep

        public IStepCondition<WorkflowStep> Configure() {
            _curentExitCondition = null;
	        _defaultNextStep = null;
            _nextStepsMap.Clear();
            return this;
        }

        #endregion
    }
}
