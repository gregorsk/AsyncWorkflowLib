﻿namespace AsyncWorkflow {
    public interface IConfigurableStep<TStep> : IStepCondition<TStep>, IStepConditionResult<TStep> where TStep:WorkflowStep {
        IStepCondition<TStep> Configure();
    }
}