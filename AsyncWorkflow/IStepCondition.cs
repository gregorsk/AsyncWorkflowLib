﻿using System;

namespace AsyncWorkflow {
    public interface IStepCondition<TStep> where TStep:WorkflowStep {
	    IStepConditionResult<TStep> When(Func<TStep, bool> condition);

		IStepConditionResult<TStep> When(StepStatus status);

	    IStepCondition<TStep> RunIf(Func<bool> condition);

        IStepCondition<TStep> SkipThisStep();

	    IStepCondition<TStep> DefaultNextStep(WorkflowStep nextStep);
	}
}