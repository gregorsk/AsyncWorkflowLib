﻿using System;
using System.Threading.Tasks;

namespace AsyncWorkflow {
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
    public class WorkflowStepWithRollback<T> : WorkflowStep<T> {
	    public WorkflowStepWithRollback(string stepName, Func<WorkflowStepWithRollback<T>, SetStepResult<T>, Task<StepStatus>> asyncFunc) : base(stepName) {
		    SetStepFunction((step, result) => asyncFunc(this, result));
	    }

		public WorkflowStep RollbackStep { get; set; }
	}
}
