﻿namespace AsyncWorkflow {
    public interface IStepConditionResult<TStep> where TStep:WorkflowStep {
        IStepCondition<TStep> GotoStep(WorkflowStep nextStep);

        IStepCondition<TStep> RepeatTimes(int i);
    }
}